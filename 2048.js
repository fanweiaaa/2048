/**
 * Created by Administrator on 2014/8/19.
 */

$(function () {
    renderRandomBlock();              //随机产生两个数字块
    bindEvents();                     //上下左右按键监听
});
//检测是否可以相加：blockAdditive
var x_arr = [".x0", ".x1", ".x2", ".x3"];
var y_arr1 = [".y3", ".y2", ".y1", ".y0"];
var y_arr = [".y0", ".y1", ".y2", ".y3"];
var x_arr1 = [".x3", ".x2", ".x1", ".x0"];

var blockMerged = false,          //数字相加标记位
    blockMoved = false,           //数字移动标记位
    full = true,                  //为true时候代表满格！
    blockAdditive = false;         //为true的时候代表可以相加

function render_a_block_in_empty() {          //在空格上生成一个2或者4的随机数字块
    var cd0, b0;                     //cd0表示坐标，color_0表示数字的颜色
    //随机产生一个坐标数
    cd0 = [produceRandomNumber(), produceRandomNumber()];
    b0 = $(".x" + cd0[0]).filter(".y" + cd0[1]);                        //随机数坐标
    var b0_text = b0.text();                                            //获取随机数的值
    while (b0_text != "") {                                             //在空白处生成2或4
        cd0 = [produceRandomNumber(), produceRandomNumber()];
        b0 = $(".x" + cd0[0]).filter(".y" + cd0[1]);                    //坐标值
        b0_text = b0.text();
    }
    b0.text(produceRandom2_4());        //设置随机数的值

};

function add_or_move(){
    if (blockMerged || blockMoved) {                      //判断数字是否移动过或者相加过，默认执行2次
        render_a_block_in_empty();
        blockMerged = false;                                                //相加标记位重置
        blockMoved = false;                                                 //移动标记位重置
    }
}

function score(x, zero, one, tow, three) {
    var x0_y3 = Number($(x)[zero].innerText);      //x0的第3个坐标值
    var x0_y2 = Number($(x)[one].innerText);      //x0的第2个坐标值
    var x0_y1 = Number($(x)[tow].innerText);      //x0的第1个坐标值
    var x0_y0 = Number($(x)[three].innerText);      //x0的第0个坐标值

    var EMPTY_BG_COLOR = "#CCC1B3";
    var EMPTY_INNER_TEXT = "";

    if (x0_y3 == "" && x0_y2 == "" && x0_y1 == x0_y0 && x0_y0 != "") {   //第1个等于第0个且其中一个不为空且2，3不为空：
        blockMerged = true;
        $(x)[zero].innerText = x0_y1 + x0_y0;                       //第3个等于第1个加上第0个  并且清空第1,0个
        if($(x)[zero].innerText==4){
            $(".fw_score").text(Number($(".fw_score").text())+4);
        }else if($(x)[zero].innerText==8){
            $(".fw_score").text(Number($(".fw_score").text())+8);
        }else if($(x)[zero].innerText==16){
            $(".fw_score").text(Number($(".fw_score").text())+16);
        }else if($(x)[zero].innerText==32){
            $(".fw_score").text(Number($(".fw_score").text())+32);
        }else if($(x)[zero].innerText==64){
            $(".fw_score").text(Number($(".fw_score").text())+64);
        }else if($(x)[zero].innerText==128){
            $(".fw_score").text(Number($(".fw_score").text())+128);
        }else if($(x)[zero].innerText==256){
            $(".fw_score").text(Number($(".fw_score").text())+256);
        }else if($(x)[zero].innerText==512){
            $(".fw_score").text(Number($(".fw_score").text())+512);
        }else if($(x)[zero].innerText==1024){
            $(".fw_score").text(Number($(".fw_score").text())+1024);
        }else if($(x)[zero].innerText==2048){
            $(".fw_score").text(Number($(".fw_score").text())+2048);
        }else if($(x)[zero].innerText==4096){
            $(".fw_score").text(Number($(".fw_score").text())+4096);
        }else if($(x)[zero].innerText==8192){
            $(".fw_score").text(Number($(".fw_score").text())+8192);
        }else if($(x)[zero].innerText==16384){
            $(".fw_score").text(Number($(".fw_score").text())+16384);
        }

        $(x)[tow].innerText = EMPTY_INNER_TEXT;
        $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
        $(x)[three].innerText = EMPTY_INNER_TEXT;
        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
    }

    if (x0_y3 == x0_y2 && x0_y3 != "") {                            //第3个等于第2个且其中一个不为空：
        blockMerged = true;
        $(x)[zero].innerText = x0_y3 + x0_y2;                       //第3个等于第3个加上第2个  并且清空第2个
        if($(x)[zero].innerText==4){
            $(".fw_score").text(Number($(".fw_score").text())+4);
        }else if($(x)[zero].innerText==8){
            $(".fw_score").text(Number($(".fw_score").text())+8);
        }else if($(x)[zero].innerText==16){
            $(".fw_score").text(Number($(".fw_score").text())+16);
        }else if($(x)[zero].innerText==32){
            $(".fw_score").text(Number($(".fw_score").text())+32);
        }else if($(x)[zero].innerText==64){
            $(".fw_score").text(Number($(".fw_score").text())+64);
        }else if($(x)[zero].innerText==128){
            $(".fw_score").text(Number($(".fw_score").text())+128);
        }else if($(x)[zero].innerText==256){
            $(".fw_score").text(Number($(".fw_score").text())+256);
        }else if($(x)[zero].innerText==512){
            $(".fw_score").text(Number($(".fw_score").text())+512);
        }else if($(x)[zero].innerText==1024){
            $(".fw_score").text(Number($(".fw_score").text())+1024);
        }else if($(x)[zero].innerText==2048){
            $(".fw_score").text(Number($(".fw_score").text())+2048);
        }else if($(x)[zero].innerText==4096){
            $(".fw_score").text(Number($(".fw_score").text())+4096);
        }else if($(x)[zero].innerText==8192){
            $(".fw_score").text(Number($(".fw_score").text())+8192);
        }else if($(x)[zero].innerText==16384){
            $(".fw_score").text(Number($(".fw_score").text())+16384);
        }

        $(x)[one].innerText = EMPTY_INNER_TEXT;
        $(x)[one].style.backgroundColor = EMPTY_BG_COLOR;

        if (x0_y1 == x0_y0 && x0_y1 != "") {                        //第1个等于第0个且其中一个不为空：
            $(x)[one].innerText = x0_y1 + x0_y0;                    //第2个等于第1个加上第0个  并且清空第1，0个
            if($(x)[one].innerText==4){
                $(".fw_score").text(Number($(".fw_score").text())+4);
            }else if($(x)[one].innerText==8){
                $(".fw_score").text(Number($(".fw_score").text())+8);
            }else if($(x)[one].innerText==16){
                $(".fw_score").text(Number($(".fw_score").text())+16);
            }else if($(x)[one].innerText==32){
                $(".fw_score").text(Number($(".fw_score").text())+32);
            }else if($(x)[one].innerText==64){
                $(".fw_score").text(Number($(".fw_score").text())+64);
            }else if($(x)[one].innerText==128){
                $(".fw_score").text(Number($(".fw_score").text())+128);
            }else if($(x)[one].innerText==256){
                $(".fw_score").text(Number($(".fw_score").text())+256);
            }else if($(x)[one].innerText==512){
                $(".fw_score").text(Number($(".fw_score").text())+512);
            }else if($(x)[one].innerText==1024){
                $(".fw_score").text(Number($(".fw_score").text())+1024);
            }else if($(x)[one].innerText==2048){
                $(".fw_score").text(Number($(".fw_score").text())+2048);
            }else if($(x)[one].innerText==4096){
                $(".fw_score").text(Number($(".fw_score").text())+4096);
            }else if($(x)[one].innerText==8192){
                $(".fw_score").text(Number($(".fw_score").text())+8192);
            }else if($(x)[one].innerText==16384){
                $(".fw_score").text(Number($(".fw_score").text())+16384);
            }

            $(x)[tow].innerText = EMPTY_INNER_TEXT;
            $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            $(x)[three].innerText = EMPTY_INNER_TEXT;
            $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
        }

    } else if (x0_y3 != x0_y2) {                                     //第3个不等于第2个：
        if (x0_y2 == "") {                                           //第2个为空：
            if (x0_y3 == x0_y1 && x0_y1 != "") {                     // 第3个等于第1个且其中一个不为空：
                blockMerged = true;
                $(x)[zero].innerText = x0_y3 + x0_y1;                //第3个等于第3个加上第1个  并且清空第1个
                if($(x)[zero].innerText==4){
                    $(".fw_score").text(Number($(".fw_score").text())+4);
                }else if($(x)[zero].innerText==8){
                    $(".fw_score").text(Number($(".fw_score").text())+8);
                }else if($(x)[zero].innerText==16){
                    $(".fw_score").text(Number($(".fw_score").text())+16);
                }else if($(x)[zero].innerText==32){
                    $(".fw_score").text(Number($(".fw_score").text())+32);
                }else if($(x)[zero].innerText==64){
                    $(".fw_score").text(Number($(".fw_score").text())+64);
                }else if($(x)[zero].innerText==128){
                    $(".fw_score").text(Number($(".fw_score").text())+128);
                }else if($(x)[zero].innerText==256){
                    $(".fw_score").text(Number($(".fw_score").text())+256);
                }else if($(x)[zero].innerText==512){
                    $(".fw_score").text(Number($(".fw_score").text())+512);
                }else if($(x)[zero].innerText==1024){
                    $(".fw_score").text(Number($(".fw_score").text())+1024);
                }else if($(x)[zero].innerText==2048){
                    $(".fw_score").text(Number($(".fw_score").text())+2048);
                }else if($(x)[zero].innerText==4096){
                    $(".fw_score").text(Number($(".fw_score").text())+4096);
                }else if($(x)[zero].innerText==8192){
                    $(".fw_score").text(Number($(".fw_score").text())+8192);
                }else if($(x)[zero].innerText==16384){
                    $(".fw_score").text(Number($(".fw_score").text())+16384);
                }

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            } else if (x0_y3 != x0_y1) {                             //第3个不等于第1个：
                if (x0_y3 == x0_y0 && x0_y1 == "" && x0_y2 == "" && x0_y3 !== "") {    //第3个等于第0个且其中一个不为空：
                    blockMerged = true;                                                //2，3都为空
                    $(x)[zero].innerText = x0_y3 + x0_y0;            //第3个等于第3个加上第0个  并且清空第0个
                    if($(x)[zero].innerText==4){
                        $(".fw_score").text(Number($(".fw_score").text())+4);
                    }else if($(x)[zero].innerText==8){
                        $(".fw_score").text(Number($(".fw_score").text())+8);
                    }else if($(x)[zero].innerText==16){
                        $(".fw_score").text(Number($(".fw_score").text())+16);
                    }else if($(x)[zero].innerText==32){
                        $(".fw_score").text(Number($(".fw_score").text())+32);
                    }else if($(x)[zero].innerText==64){
                        $(".fw_score").text(Number($(".fw_score").text())+64);
                    }else if($(x)[zero].innerText==128){
                        $(".fw_score").text(Number($(".fw_score").text())+128);
                    }else if($(x)[zero].innerText==256){
                        $(".fw_score").text(Number($(".fw_score").text())+256);
                    }else if($(x)[zero].innerText==512){
                        $(".fw_score").text(Number($(".fw_score").text())+512);
                    }else if($(x)[zero].innerText==1024){
                        $(".fw_score").text(Number($(".fw_score").text())+1024);
                    }else if($(x)[zero].innerText==2048){
                        $(".fw_score").text(Number($(".fw_score").text())+2048);
                    }else if($(x)[zero].innerText==4096){
                        $(".fw_score").text(Number($(".fw_score").text())+4096);
                    }else if($(x)[zero].innerText==8192){
                        $(".fw_score").text(Number($(".fw_score").text())+8192);
                    }else if($(x)[zero].innerText==16384){
                        $(".fw_score").text(Number($(".fw_score").text())+16384);
                    }

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
                if (x0_y1 == x0_y0 && x0_y0 != "") {                //第1个等于第0个且其中一个不为空：
                    blockMerged = true;
                    $(x)[one].innerText = x0_y1 + x0_y0;            //第2个等于第1个加上第0个  并且清空第1，0个
                    if($(x)[one].innerText==4){
                        $(".fw_score").text(Number($(".fw_score").text())+4);
                    }else if($(x)[one].innerText==8){
                        $(".fw_score").text(Number($(".fw_score").text())+8);
                    }else if($(x)[one].innerText==16){
                        $(".fw_score").text(Number($(".fw_score").text())+16);
                    }else if($(x)[one].innerText==32){
                        $(".fw_score").text(Number($(".fw_score").text())+32);
                    }else if($(x)[one].innerText==64){
                        $(".fw_score").text(Number($(".fw_score").text())+64);
                    }else if($(x)[one].innerText==128){
                        $(".fw_score").text(Number($(".fw_score").text())+128);
                    }else if($(x)[one].innerText==256){
                        $(".fw_score").text(Number($(".fw_score").text())+256);
                    }else if($(x)[one].innerText==512){
                        $(".fw_score").text(Number($(".fw_score").text())+512);
                    }else if($(x)[one].innerText==1024){
                        $(".fw_score").text(Number($(".fw_score").text())+1024);
                    }else if($(x)[one].innerText==2048){
                        $(".fw_score").text(Number($(".fw_score").text())+2048);
                    }else if($(x)[one].innerText==4096){
                        $(".fw_score").text(Number($(".fw_score").text())+4096);
                    }else if($(x)[one].innerText==8192){
                        $(".fw_score").text(Number($(".fw_score").text())+8192);
                    }else if($(x)[one].innerText==16384){
                        $(".fw_score").text(Number($(".fw_score").text())+16384);
                    }

                    $(x)[tow].innerText = EMPTY_INNER_TEXT;
                    $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            }
        } else if (x0_y2 != "") {                               //第2个不为空：
            if (x0_y2 == x0_y1 && x0_y1 != "") {                //第2个等于第1个且其中一个不为空：
                blockMerged = true;
                $(x)[one].innerText = x0_y2 + x0_y1;                // 第2个等于第2个加上第1个  并且清空第1个
                if($(x)[one].innerText==4){
                    $(".fw_score").text(Number($(".fw_score").text())+4);
                }else if($(x)[one].innerText==8){
                    $(".fw_score").text(Number($(".fw_score").text())+8);
                }else if($(x)[one].innerText==16){
                    $(".fw_score").text(Number($(".fw_score").text())+16);
                }else if($(x)[one].innerText==32){
                    $(".fw_score").text(Number($(".fw_score").text())+32);
                }else if($(x)[one].innerText==64){
                    $(".fw_score").text(Number($(".fw_score").text())+64);
                }else if($(x)[one].innerText==128){
                    $(".fw_score").text(Number($(".fw_score").text())+128);
                }else if($(x)[one].innerText==256){
                    $(".fw_score").text(Number($(".fw_score").text())+256);
                }else if($(x)[one].innerText==512){
                    $(".fw_score").text(Number($(".fw_score").text())+512);
                }else if($(x)[one].innerText==1024){
                    $(".fw_score").text(Number($(".fw_score").text())+1024);
                }else if($(x)[one].innerText==2048){
                    $(".fw_score").text(Number($(".fw_score").text())+2048);
                }else if($(x)[one].innerText==4096){
                    $(".fw_score").text(Number($(".fw_score").text())+4096);
                }else if($(x)[one].innerText==8192){
                    $(".fw_score").text(Number($(".fw_score").text())+8192);
                }else if($(x)[one].innerText==16384){
                    $(".fw_score").text(Number($(".fw_score").text())+16384);
                }

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            } else if (x0_y2 != x0_y1) {                          //第2个不等于第1个：
                if (x0_y1 == "") {                                // 第1个为空：
                    if (x0_y2 == x0_y0 && x0_y0 != "") {          // 第2个等于第0个且其中一个不为空：
                        blockMerged = true;
                        $(x)[one].innerText = x0_y2 + x0_y0;      // 第2个等于第2个加上第0个 并且清空第0个
                        if($(x)[one].innerText==4){
                            $(".fw_score").text(Number($(".fw_score").text())+4);
                        }else if($(x)[one].innerText==8){
                            $(".fw_score").text(Number($(".fw_score").text())+8);
                        }else if($(x)[one].innerText==16){
                            $(".fw_score").text(Number($(".fw_score").text())+16);
                        }else if($(x)[one].innerText==32){
                            $(".fw_score").text(Number($(".fw_score").text())+32);
                        }else if($(x)[one].innerText==64){
                            $(".fw_score").text(Number($(".fw_score").text())+64);
                        }else if($(x)[one].innerText==128){
                            $(".fw_score").text(Number($(".fw_score").text())+128);
                        }else if($(x)[one].innerText==256){
                            $(".fw_score").text(Number($(".fw_score").text())+256);
                        }else if($(x)[one].innerText==512){
                            $(".fw_score").text(Number($(".fw_score").text())+512);
                        }else if($(x)[one].innerText==1024){
                            $(".fw_score").text(Number($(".fw_score").text())+1024);
                        }else if($(x)[one].innerText==2048){
                            $(".fw_score").text(Number($(".fw_score").text())+2048);
                        }else if($(x)[one].innerText==4096){
                            $(".fw_score").text(Number($(".fw_score").text())+4096);
                        }else if($(x)[one].innerText==8192){
                            $(".fw_score").text(Number($(".fw_score").text())+8192);
                        }else if($(x)[one].innerText==16384){
                            $(".fw_score").text(Number($(".fw_score").text())+16384);
                        }

                        $(x)[three].innerText = EMPTY_INNER_TEXT;
                        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                    }
                } else if (x0_y1 != "") {                       //第1个不为空：
                    if (x0_y1 == x0_y0 && x0_y0 != "") {        //第1个等于第0个：
                        blockMerged = true;
                        $(x)[tow].innerText = x0_y1 + x0_y0;    //第1个等于第1个加上第0个  并且清空第0个
                        if($(x)[tow].innerText==4){
                            $(".fw_score").text(Number($(".fw_score").text())+4);
                        }else if($(x)[tow].innerText==8){
                            $(".fw_score").text(Number($(".fw_score").text())+8);
                        }else if($(x)[tow].innerText==16){
                            $(".fw_score").text(Number($(".fw_score").text())+16);
                        }else if($(x)[tow].innerText==32){
                            $(".fw_score").text(Number($(".fw_score").text())+32);
                        }else if($(x)[tow].innerText==64){
                            $(".fw_score").text(Number($(".fw_score").text())+64);
                        }else if($(x)[tow].innerText==128){
                            $(".fw_score").text(Number($(".fw_score").text())+128);
                        }else if($(x)[tow].innerText==256){
                            $(".fw_score").text(Number($(".fw_score").text())+256);
                        }else if($(x)[tow].innerText==512){
                            $(".fw_score").text(Number($(".fw_score").text())+512);
                        }else if($(x)[tow].innerText==1024){
                            $(".fw_score").text(Number($(".fw_score").text())+1024);
                        }else if($(x)[tow].innerText==2048){
                            $(".fw_score").text(Number($(".fw_score").text())+2048);
                        }else if($(x)[tow].innerText==4096){
                            $(".fw_score").text(Number($(".fw_score").text())+4096);
                        }else if($(x)[tow].innerText==8192){
                            $(".fw_score").text(Number($(".fw_score").text())+8192);
                        }else if($(x)[tow].innerText==16384){
                            $(".fw_score").text(Number($(".fw_score").text())+16384);
                        }

                        $(x)[three].innerText = EMPTY_INNER_TEXT;
                        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                    }
                }
            }
        }
    }
}


function additive(x, zero, one, tow, three) {
    var x0_y3 = Number($(x)[zero].innerText);      //x0的第3个坐标值
    var x0_y2 = Number($(x)[one].innerText);      //x0的第2个坐标值
    var x0_y1 = Number($(x)[tow].innerText);      //x0的第1个坐标值
    var x0_y0 = Number($(x)[three].innerText);      //x0的第0个坐标值


    if (x0_y3 == "" && x0_y2 == "" && x0_y1 == x0_y0 && x0_y0 != "") {   //第1个等于第0个且其中一个不为空且2，3不为空：
        blockAdditive = true;
    }

    if (x0_y3 == x0_y2 && x0_y3 != "") {                            //第3个等于第2个且其中一个不为空：
        blockAdditive = true;

        if (x0_y1 == x0_y0 && x0_y1 != "") {                        //第1个等于第0个且其中一个不为空：
            blockAdditive = true;
        }

    } else if (x0_y3 != x0_y2) {                                     //第3个不等于第2个：
        if (x0_y2 == "") {                                           //第2个为空：
            if (x0_y3 == x0_y1 && x0_y1 != "") {                     // 第3个等于第1个且其中一个不为空：
                blockAdditive = true;
            } else if (x0_y3 != x0_y1) {                             //第3个不等于第1个：
                if (x0_y3 == x0_y0 && x0_y1 == "" && x0_y2 == "" && x0_y3 !== "") {    //第3个等于第0个且其中一个不为空：
                    blockAdditive = true;
                }
                if (x0_y1 == x0_y0 && x0_y0 != "") {                //第1个等于第0个且其中一个不为空：
                    blockAdditive = true;
                }
            }
        } else if (x0_y2 != "") {                               //第2个不为空：
            if (x0_y2 == x0_y1 && x0_y1 != "") {                //第2个等于第1个且其中一个不为空：
                blockAdditive = true;
            } else if (x0_y2 != x0_y1) {                          //第2个不等于第1个：
                if (x0_y1 == "") {                                // 第1个为空：
                    if (x0_y2 == x0_y0 && x0_y0 != "") {          // 第2个等于第0个且其中一个不为空：
                        blockAdditive = true;
                    }
                } else if (x0_y1 != "") {                       //第1个不为空：
                    if (x0_y1 == x0_y0 && x0_y0 != "") {        //第1个等于第0个：
                        blockAdditive = true;
                    }
                }
            }
        }
    }
}


function game_over() {
    //检测是否可以相加
    for (var i = 0; i < 4; i++) {
        additive(x_arr[i], 0, 1, 2, 3);
        additive(y_arr1[i], 0, 1, 2, 3);
        additive(y_arr[i], 3, 2, 1, 0);
        additive(x_arr1[i], 3, 2, 1, 0);
    }

    //判断是否还有空格：full
    var $td = $("td");
    for (var i = 0; i < $td.length; i++) {
        if ($td[i].innerText == "") {
            full = false;
        }
    }
    /*
     * full = true,      //为true时候代表满格！
     blockAdditive = false,//为true的时候代表可以相加
     */
    //满格并且不能相加full=true&&blockAdditive == false;
    if (full && blockAdditive == false) {
        alert("game_over");
    }

    //???
    full = true;
    blockAdditive = false;
}


function change_color() {           //更改数字颜色
    var $td = $("td");
    for (var i = 0; i < $td.length; i++) {
        if ($td[i].innerText == "") {
            $td[i].style.backgroundColor = "#CCC1B3";
        } else if ($td[i].innerText == "2") {
            $td[i].style.backgroundColor = "#EDE5D9";
        } else if ($td[i].innerText == "4") {
            $td[i].style.backgroundColor = "#EDE57E";
        } else if ($td[i].innerText == "8") {
            $td[i].style.backgroundColor = "#F4B075";
        } else if ($td[i].innerText == "16") {
            $td[i].style.backgroundColor = "#F4B099";
        } else if ($td[i].innerText == "32") {
            $td[i].style.backgroundColor = "#F97C5A";
        } else if ($td[i].innerText == "64") {
            $td[i].style.backgroundColor = "#F75C31";
        } else if ($td[i].innerText == "128") {
            $td[i].style.backgroundColor = "#EECD6B";
        } else if ($td[i].innerText == "256") {
            $td[i].style.backgroundColor = "#E7CC63";
        } else if ($td[i].innerText == "512") {
            $td[i].style.backgroundColor = "#E7CA4C";
        } else if ($td[i].innerText == "1024") {
            $td[i].style.backgroundColor = "#E9C437";
        } else if ($td[i].innerText == "2048") {
            $td[i].style.backgroundColor = "#E6C224";
        } else if ($td[i].innerText == "4096") {
            $td[i].style.backgroundColor = "#AF83A6";
        } else if ($td[i].innerText == "8192") {
            $td[i].style.backgroundColor = "#AD71A7";
        } else if ($td[i].innerText >= "16384") {
            $td[i].style.backgroundColor = "#995795";
        }
    }

}


function renderRandomBlock() {              //随机生成两个随机数
    render_a_block_in_empty();
    render_a_block_in_empty();
    change_color();
};


function bindEvents() {                     //绑定上下左右,重新开始按钮的监听事件
    $(document).keyup(function (event) {
        if (event.which == 37) {
            leftHandler();
        } else if (event.which == 38) {
            topHandler();
        } else if (event.which == 39) {
            rightHandler();
        } else if (event.which == 40) {
            bottomHandler();
        }
        add_or_move()

        change_color();                      //更改颜色
        game_over();                         //判断游戏是否结束
    });

    $(".fw_restart").click(function () {
        console.log($(window));
        $(window)[0].history.go(0);
    })
};

function topHandler() {                         //向上功能键
    for (var i = 0; i < 4; i++) {
        mergeNumber(x_arr[i], 0, 1, 2, 3);
        moveToTop(x_arr[i], 0, 1, 2, 3);
    }

};

function leftHandler() {                        //向左功能键
    for (var i = 0; i < 4; i++) {
        mergeNumber(y_arr1[i], 0, 1, 2, 3);
        moveToTop(y_arr1[i], 0, 1, 2, 3);
    }
};

function rightHandler() {                           //向右功能键
    for (var i = 0; i < 4; i++) {
        mergeNumber(y_arr[i], 3, 2, 1, 0);
        moveToTop(y_arr[i], 3, 2, 1, 0);
    }
};

function bottomHandler() {                      //向下功能键
    for (var i = 0; i < 4; i++) {
        mergeNumber(x_arr1[i], 3, 2, 1, 0);
        moveToTop(x_arr1[i], 3, 2, 1, 0);
    }
};

function produceRandomNumber() {             //随机产生坐标数
    var ret, r;

    r = Math.random();
    if (r <= 0.25) {
        ret = 0;
    } else if (r > 0.25 && r <= 0.5) {
        ret = 1;
    } else if (r > 0.5 && r <= 0.75) {
        ret = 2;
    } else {
        ret = 3;
    }

    return ret;
};

function produceRandom2_4() {               //随机产生数字 4和4
    var ret, r;

    r = Math.random();
    if (r <= 0.75) {
        ret = 2;
    } else {
        ret = 4;
    }

    return ret;
};

function mergeNumber(x, zero, one, tow, three) {                       //数字合并功能
    var x0_y3 = Number($(x)[zero].innerText);      //x0的第3个坐标值
    var x0_y2 = Number($(x)[one].innerText);      //x0的第2个坐标值
    var x0_y1 = Number($(x)[tow].innerText);      //x0的第1个坐标值
    var x0_y0 = Number($(x)[three].innerText);      //x0的第0个坐标值

    var EMPTY_BG_COLOR = "#CCC1B3";
    var EMPTY_INNER_TEXT = "";

    if (x0_y3 == "" && x0_y2 == "" && x0_y1 == x0_y0 && x0_y0 != "") {   //第1个等于第0个且其中一个不为空且2，3不为空：
        blockMerged = true;
        $(x)[zero].innerText= x0_y1 + x0_y0;                       //第3个等于第1个加上第0个  并且清空第1,0个
        if($(x)[zero].innerText==4){
            $(".fw_score").text(Number($(".fw_score").text())+4);
        }else if($(x)[zero].innerText==8){
            $(".fw_score").text(Number($(".fw_score").text())+8);
        }else if($(x)[zero].innerText==16){
            $(".fw_score").text(Number($(".fw_score").text())+16);
        }else if($(x)[zero].innerText==32){
            $(".fw_score").text(Number($(".fw_score").text())+32);
        }else if($(x)[zero].innerText==64){
            $(".fw_score").text(Number($(".fw_score").text())+64);
        }else if($(x)[zero].innerText==128){
            $(".fw_score").text(Number($(".fw_score").text())+128);
        }else if($(x)[zero].innerText==256){
            $(".fw_score").text(Number($(".fw_score").text())+256);
        }else if($(x)[zero].innerText==512){
            $(".fw_score").text(Number($(".fw_score").text())+512);
        }else if($(x)[zero].innerText==1024){
            $(".fw_score").text(Number($(".fw_score").text())+1024);
        }else if($(x)[zero].innerText==2048){
            $(".fw_score").text(Number($(".fw_score").text())+2048);
        }else if($(x)[zero].innerText==4096){
            $(".fw_score").text(Number($(".fw_score").text())+4096);
        }else if($(x)[zero].innerText==8192){
            $(".fw_score").text(Number($(".fw_score").text())+8192);
        }else if($(x)[zero].innerText==16384){
            $(".fw_score").text(Number($(".fw_score").text())+16384);
        }

        $(x)[tow].innerText = EMPTY_INNER_TEXT;
        $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
        $(x)[three].innerText = EMPTY_INNER_TEXT;
        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
    }

    if (x0_y3 == x0_y2 && x0_y3 != "") {                            //第3个等于第2个且其中一个不为空：
        blockMerged = true;
        $(x)[zero].innerText = x0_y3 + x0_y2;                       //第3个等于第3个加上第2个  并且清空第2个
        if($(x)[zero].innerText==4){
            $(".fw_score").text(Number($(".fw_score").text())+4);
        }else if($(x)[zero].innerText==8){
            $(".fw_score").text(Number($(".fw_score").text())+8);
        }else if($(x)[zero].innerText==16){
            $(".fw_score").text(Number($(".fw_score").text())+16);
        }else if($(x)[zero].innerText==32){
            $(".fw_score").text(Number($(".fw_score").text())+32);
        }else if($(x)[zero].innerText==64){
            $(".fw_score").text(Number($(".fw_score").text())+64);
        }else if($(x)[zero].innerText==128){
            $(".fw_score").text(Number($(".fw_score").text())+128);
        }else if($(x)[zero].innerText==256){
            $(".fw_score").text(Number($(".fw_score").text())+256);
        }else if($(x)[zero].innerText==512){
            $(".fw_score").text(Number($(".fw_score").text())+512);
        }else if($(x)[zero].innerText==1024){
            $(".fw_score").text(Number($(".fw_score").text())+1024);
        }else if($(x)[zero].innerText==2048){
            $(".fw_score").text(Number($(".fw_score").text())+2048);
        }else if($(x)[zero].innerText==4096){
            $(".fw_score").text(Number($(".fw_score").text())+4096);
        }else if($(x)[zero].innerText==8192){
            $(".fw_score").text(Number($(".fw_score").text())+8192);
        }else if($(x)[zero].innerText==16384){
            $(".fw_score").text(Number($(".fw_score").text())+16384);
        }

        $(x)[one].innerText = EMPTY_INNER_TEXT;
        $(x)[one].style.backgroundColor = EMPTY_BG_COLOR;

        if (x0_y1 == x0_y0 && x0_y1 != "") {                        //第1个等于第0个且其中一个不为空：
            $(x)[one].innerText = x0_y1 + x0_y0;                    //第2个等于第1个加上第0个  并且清空第1，0个
            if($(x)[one].innerText==4){
                $(".fw_score").text(Number($(".fw_score").text())+4);
            }else if($(x)[one].innerText==8){
                $(".fw_score").text(Number($(".fw_score").text())+8);
            }else if($(x)[one].innerText==16){
                $(".fw_score").text(Number($(".fw_score").text())+16);
            }else if($(x)[one].innerText==32){
                $(".fw_score").text(Number($(".fw_score").text())+32);
            }else if($(x)[one].innerText==64){
                $(".fw_score").text(Number($(".fw_score").text())+64);
            }else if($(x)[one].innerText==128){
                $(".fw_score").text(Number($(".fw_score").text())+128);
            }else if($(x)[one].innerText==256){
                $(".fw_score").text(Number($(".fw_score").text())+256);
            }else if($(x)[one].innerText==512){
                $(".fw_score").text(Number($(".fw_score").text())+512);
            }else if($(x)[one].innerText==1024){
                $(".fw_score").text(Number($(".fw_score").text())+1024);
            }else if($(x)[one].innerText==2048){
                $(".fw_score").text(Number($(".fw_score").text())+2048);
            }else if($(x)[one].innerText==4096){
                $(".fw_score").text(Number($(".fw_score").text())+4096);
            }else if($(x)[one].innerText==8192){
                $(".fw_score").text(Number($(".fw_score").text())+8192);
            }else if($(x)[one].innerText==16384){
                $(".fw_score").text(Number($(".fw_score").text())+16384);
            }

            $(x)[tow].innerText = EMPTY_INNER_TEXT;
            $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            $(x)[three].innerText = EMPTY_INNER_TEXT;
            $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
        }

    } else if (x0_y3 != x0_y2) {                                     //第3个不等于第2个：
        if (x0_y2 == "") {                                           //第2个为空：
            if (x0_y3 == x0_y1 && x0_y1 != "") {                     // 第3个等于第1个且其中一个不为空：
                blockMerged = true;
                $(x)[zero].innerText = x0_y3 + x0_y1;                //第3个等于第3个加上第1个  并且清空第1个
                if($(x)[zero].innerText==4){
                    $(".fw_score").text(Number($(".fw_score").text())+4);
                }else if($(x)[zero].innerText==8){
                    $(".fw_score").text(Number($(".fw_score").text())+8);
                }else if($(x)[zero].innerText==16){
                    $(".fw_score").text(Number($(".fw_score").text())+16);
                }else if($(x)[zero].innerText==32){
                    $(".fw_score").text(Number($(".fw_score").text())+32);
                }else if($(x)[zero].innerText==64){
                    $(".fw_score").text(Number($(".fw_score").text())+64);
                }else if($(x)[zero].innerText==128){
                    $(".fw_score").text(Number($(".fw_score").text())+128);
                }else if($(x)[zero].innerText==256){
                    $(".fw_score").text(Number($(".fw_score").text())+256);
                }else if($(x)[zero].innerText==512){
                    $(".fw_score").text(Number($(".fw_score").text())+512);
                }else if($(x)[zero].innerText==1024){
                    $(".fw_score").text(Number($(".fw_score").text())+1024);
                }else if($(x)[zero].innerText==2048){
                    $(".fw_score").text(Number($(".fw_score").text())+2048);
                }else if($(x)[zero].innerText==4096){
                    $(".fw_score").text(Number($(".fw_score").text())+4096);
                }else if($(x)[zero].innerText==8192){
                    $(".fw_score").text(Number($(".fw_score").text())+8192);
                }else if($(x)[zero].innerText==16384){
                    $(".fw_score").text(Number($(".fw_score").text())+16384);
                }

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            } else if (x0_y3 != x0_y1) {                             //第3个不等于第1个：
                if (x0_y3 == x0_y0 && x0_y1 == "" && x0_y2 == "" && x0_y3 !== "") {    //第3个等于第0个且其中一个不为空：
                    blockMerged = true;                                                //2，3都为空
                    $(x)[zero].innerText = x0_y3 + x0_y0;            //第3个等于第3个加上第0个  并且清空第0个
                    if($(x)[zero].innerText==4){
                        $(".fw_score").text(Number($(".fw_score").text())+4);
                    }else if($(x)[zero].innerText==8){
                        $(".fw_score").text(Number($(".fw_score").text())+8);
                    }else if($(x)[zero].innerText==16){
                        $(".fw_score").text(Number($(".fw_score").text())+16);
                    }else if($(x)[zero].innerText==32){
                        $(".fw_score").text(Number($(".fw_score").text())+32);
                    }else if($(x)[zero].innerText==64){
                        $(".fw_score").text(Number($(".fw_score").text())+64);
                    }else if($(x)[zero].innerText==128){
                        $(".fw_score").text(Number($(".fw_score").text())+128);
                    }else if($(x)[zero].innerText==256){
                        $(".fw_score").text(Number($(".fw_score").text())+256);
                    }else if($(x)[zero].innerText==512){
                        $(".fw_score").text(Number($(".fw_score").text())+512);
                    }else if($(x)[zero].innerText==1024){
                        $(".fw_score").text(Number($(".fw_score").text())+1024);
                    }else if($(x)[zero].innerText==2048){
                        $(".fw_score").text(Number($(".fw_score").text())+2048);
                    }else if($(x)[zero].innerText==4096){
                        $(".fw_score").text(Number($(".fw_score").text())+4096);
                    }else if($(x)[zero].innerText==8192){
                        $(".fw_score").text(Number($(".fw_score").text())+8192);
                    }else if($(x)[zero].innerText==16384){
                        $(".fw_score").text(Number($(".fw_score").text())+16384);
                    }

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
                if (x0_y1 == x0_y0 && x0_y0 != "") {                //第1个等于第0个且其中一个不为空：
                    blockMerged = true;
                    $(x)[one].innerText = x0_y1 + x0_y0;            //第2个等于第1个加上第0个  并且清空第1，0个
                    if($(x)[one].innerText==4){
                        $(".fw_score").text(Number($(".fw_score").text())+4);
                    }else if($(x)[one].innerText==8){
                        $(".fw_score").text(Number($(".fw_score").text())+8);
                    }else if($(x)[one].innerText==16){
                        $(".fw_score").text(Number($(".fw_score").text())+16);
                    }else if($(x)[one].innerText==32){
                        $(".fw_score").text(Number($(".fw_score").text())+32);
                    }else if($(x)[one].innerText==64){
                        $(".fw_score").text(Number($(".fw_score").text())+64);
                    }else if($(x)[one].innerText==128){
                        $(".fw_score").text(Number($(".fw_score").text())+128);
                    }else if($(x)[one].innerText==256){
                        $(".fw_score").text(Number($(".fw_score").text())+256);
                    }else if($(x)[one].innerText==512){
                        $(".fw_score").text(Number($(".fw_score").text())+512);
                    }else if($(x)[one].innerText==1024){
                        $(".fw_score").text(Number($(".fw_score").text())+1024);
                    }else if($(x)[one].innerText==2048){
                        $(".fw_score").text(Number($(".fw_score").text())+2048);
                    }else if($(x)[one].innerText==4096){
                        $(".fw_score").text(Number($(".fw_score").text())+4096);
                    }else if($(x)[one].innerText==8192){
                        $(".fw_score").text(Number($(".fw_score").text())+8192);
                    }else if($(x)[one].innerText==16384){
                        $(".fw_score").text(Number($(".fw_score").text())+16384);
                    }

                    $(x)[tow].innerText = EMPTY_INNER_TEXT;
                    $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            }
        } else if (x0_y2 != "") {                               //第2个不为空：
            if (x0_y2 == x0_y1 && x0_y1 != "") {                //第2个等于第1个且其中一个不为空：
                blockMerged = true;
                $(x)[one].innerText = x0_y2 + x0_y1;                // 第2个等于第2个加上第1个  并且清空第1个
                if($(x)[one].innerText==4){
                    $(".fw_score").text(Number($(".fw_score").text())+4);
                }else if($(x)[one].innerText==8){
                    $(".fw_score").text(Number($(".fw_score").text())+8);
                }else if($(x)[one].innerText==16){
                    $(".fw_score").text(Number($(".fw_score").text())+16);
                }else if($(x)[one].innerText==32){
                    $(".fw_score").text(Number($(".fw_score").text())+32);
                }else if($(x)[one].innerText==64){
                    $(".fw_score").text(Number($(".fw_score").text())+64);
                }else if($(x)[one].innerText==128){
                    $(".fw_score").text(Number($(".fw_score").text())+128);
                }else if($(x)[one].innerText==256){
                    $(".fw_score").text(Number($(".fw_score").text())+256);
                }else if($(x)[one].innerText==512){
                    $(".fw_score").text(Number($(".fw_score").text())+512);
                }else if($(x)[one].innerText==1024){
                    $(".fw_score").text(Number($(".fw_score").text())+1024);
                }else if($(x)[one].innerText==2048){
                    $(".fw_score").text(Number($(".fw_score").text())+2048);
                }else if($(x)[one].innerText==4096){
                    $(".fw_score").text(Number($(".fw_score").text())+4096);
                }else if($(x)[one].innerText==8192){
                    $(".fw_score").text(Number($(".fw_score").text())+8192);
                }else if($(x)[one].innerText==16384){
                    $(".fw_score").text(Number($(".fw_score").text())+16384);
                }

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;
            } else if (x0_y2 != x0_y1) {                          //第2个不等于第1个：
                if (x0_y1 == "") {                                // 第1个为空：
                    if (x0_y2 == x0_y0 && x0_y0 != "") {          // 第2个等于第0个且其中一个不为空：
                        blockMerged = true;
                        $(x)[one].innerText = x0_y2 + x0_y0;      // 第2个等于第2个加上第0个 并且清空第0个
                        if($(x)[one].innerText==4){
                            $(".fw_score").text(Number($(".fw_score").text())+4);
                        }else if($(x)[one].innerText==8){
                            $(".fw_score").text(Number($(".fw_score").text())+8);
                        }else if($(x)[one].innerText==16){
                            $(".fw_score").text(Number($(".fw_score").text())+16);
                        }else if($(x)[one].innerText==32){
                            $(".fw_score").text(Number($(".fw_score").text())+32);
                        }else if($(x)[one].innerText==64){
                            $(".fw_score").text(Number($(".fw_score").text())+64);
                        }else if($(x)[one].innerText==128){
                            $(".fw_score").text(Number($(".fw_score").text())+128);
                        }else if($(x)[one].innerText==256){
                            $(".fw_score").text(Number($(".fw_score").text())+256);
                        }else if($(x)[one].innerText==512){
                            $(".fw_score").text(Number($(".fw_score").text())+512);
                        }else if($(x)[one].innerText==1024){
                            $(".fw_score").text(Number($(".fw_score").text())+1024);
                        }else if($(x)[one].innerText==2048){
                            $(".fw_score").text(Number($(".fw_score").text())+2048);
                        }else if($(x)[one].innerText==4096){
                            $(".fw_score").text(Number($(".fw_score").text())+4096);
                        }else if($(x)[one].innerText==8192){
                            $(".fw_score").text(Number($(".fw_score").text())+8192);
                        }else if($(x)[one].innerText==16384){
                            $(".fw_score").text(Number($(".fw_score").text())+16384);
                        }

                        $(x)[three].innerText = EMPTY_INNER_TEXT;
                        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                    }
                } else if (x0_y1 != "") {                       //第1个不为空：
                    if (x0_y1 == x0_y0 && x0_y0 != "") {        //第1个等于第0个：
                        blockMerged = true;
                        $(x)[tow].innerText = x0_y1 + x0_y0;    //第1个等于第1个加上第0个  并且清空第0个
                        if($(x)[tow].innerText==4){
                            $(".fw_score").text(Number($(".fw_score").text())+4);
                        }else if($(x)[tow].innerText==8){
                            $(".fw_score").text(Number($(".fw_score").text())+8);
                        }else if($(x)[tow].innerText==16){
                            $(".fw_score").text(Number($(".fw_score").text())+16);
                        }else if($(x)[tow].innerText==32){
                            $(".fw_score").text(Number($(".fw_score").text())+32);
                        }else if($(x)[tow].innerText==64){
                            $(".fw_score").text(Number($(".fw_score").text())+64);
                        }else if($(x)[tow].innerText==128){
                            $(".fw_score").text(Number($(".fw_score").text())+128);
                        }else if($(x)[tow].innerText==256){
                            $(".fw_score").text(Number($(".fw_score").text())+256);
                        }else if($(x)[tow].innerText==512){
                            $(".fw_score").text(Number($(".fw_score").text())+512);
                        }else if($(x)[tow].innerText==1024){
                            $(".fw_score").text(Number($(".fw_score").text())+1024);
                        }else if($(x)[tow].innerText==2048){
                            $(".fw_score").text(Number($(".fw_score").text())+2048);
                        }else if($(x)[tow].innerText==4096){
                            $(".fw_score").text(Number($(".fw_score").text())+4096);
                        }else if($(x)[tow].innerText==8192){
                            $(".fw_score").text(Number($(".fw_score").text())+8192);
                        }else if($(x)[tow].innerText==16384){
                            $(".fw_score").text(Number($(".fw_score").text())+16384);
                        }

                        $(x)[three].innerText = EMPTY_INNER_TEXT;
                        $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                    }
                }
            }
        }
    }
};

function moveToTop(x, zero, one, tow, three) {        //上移功能
    var x0_y3 = $(x)[zero].innerText;      //x0的第3个坐标值
    var x0_y2 = $(x)[one].innerText;      //x0的第2个坐标值
    var x0_y1 = $(x)[tow].innerText;      //x0的第1个坐标值
    var x0_y0 = $(x)[three].innerText;      //x0的第0个坐标值

    var EMPTY_BG_COLOR = "#fff";
    var EMPTY_INNER_TEXT = "";
    if (x0_y3 == "") {                      //第3个为空：
        if (x0_y2 == "") {                  //第2个为空：
            if (x0_y1 == "") {              //第1个为空：
                if (x0_y0 == "") {          //第0个为空：
                } else if (x0_y0 != "") {   //第0个不为空：把第0个值设到第3个，并且清空第0个。
                    blockMoved = true;
                    $(x)[zero].innerText = $(x)[three].innerText;
                    $(x)[zero].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            } else if (x0_y1 != "") {       // 第1个不为空：
                blockMoved = true;
                $(x)[zero].innerText = $(x)[tow].innerText;     //把第1个值设到第3个，并且清空第1个。
                $(x)[zero].style.backgroundColor = $(x)[tow].style.backgroundColor;

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;

                if (x0_y0 == "") {          //第0个为空：
                } else if (x0_y0 != "") {   //第0个不为空：
                    blockMoved = true;
                    $(x)[one].innerText = $(x)[three].innerText;        //把第0个值设到第2个，并且清空第0个。
                    $(x)[one].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            }
        } else if (x0_y2 != "") {       //第2个不为空：
            blockMoved = true;
            $(x)[zero].innerText = $(x)[one].innerText;      //把第2个值设到第3个，并且清空第2个。
            $(x)[zero].style.backgroundColor = $(x)[one].style.backgroundColor;

            $(x)[one].innerText = EMPTY_INNER_TEXT;
            $(x)[one].style.backgroundColor = EMPTY_BG_COLOR;
            if (x0_y1 == "") {              //第1个为空：
                if (x0_y0 == "") {          //第0个为空：

                } else if (x0_y0 != "") {   //第0个不为空：
                    blockMoved = true;
                    $(x)[one].innerText = $(x)[three].innerText;    //把第0个值设到第2个，并且清空第0个。
                    $(x)[one].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }

            } else if (x0_y1 != "") {       //第1个不为空：
                blockMoved = true;
                $(x)[one].innerText = $(x)[tow].innerText;      //把第1个值设到第2个，并且清空第1个。
                $(x)[one].style.backgroundColor = $(x)[tow].style.backgroundColor;

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;

                if (x0_y0 == "") {          //第0个为空：

                } else if (x0_y0 != "") {   // 第0个不为空：
                    blockMoved = true;
                    $(x)[tow].innerText = $(x)[three].innerText;        //把第0个值设到第1个，并且清空第0个。
                    $(x)[tow].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            }
        }
    } else if (x0_y3 != "") {                   //第3个不为空：
        if (x0_y2 == "") {                      //第2个为空：
            if (x0_y1 == "") {                  //第1个为空：
                if (x0_y0 == "") {              //第0个为空：

                } else if (x0_y0 != "") {       //第0个不为空：
                    blockMoved = true;
                    $(x)[one].innerText = $(x)[three].innerText;        //把第0个值设到第2个，并且清空第0个。
                    $(x)[one].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }

            } else if (x0_y1 != "") {       // 第1个不为空：
                blockMoved = true;
                $(x)[one].innerText = $(x)[tow].innerText;      //把第1个值设到第2个，并且清空第1个。
                $(x)[one].style.backgroundColor = $(x)[tow].style.backgroundColor;

                $(x)[tow].innerText = EMPTY_INNER_TEXT;
                $(x)[tow].style.backgroundColor = EMPTY_BG_COLOR;

                if (x0_y0 == "") {          //第0个为空：

                } else if (x0_y0 != "") {   //第0个不为空：
                    blockMoved = true;
                    $(x)[tow].innerText = $(x)[three].innerText;        //把第0个值设到第1个，并且清空第0个。
                    $(x)[tow].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }
            }

        } else if (x0_y2 != "") {       //第2个不为空：
            if (x0_y1 == "") {          //第1个为空：
                if (x0_y0 == "") {      //第0个为空：

                } else if (x0_y0 != "") {       //第0个不为空：
                    blockMoved = true;
                    $(x)[tow].innerText = $(x)[three].innerText;       //把第0个值设到第1个，并且清空第0个。
                    $(x)[tow].style.backgroundColor = $(x)[three].style.backgroundColor;

                    $(x)[three].innerText = EMPTY_INNER_TEXT;
                    $(x)[three].style.backgroundColor = EMPTY_BG_COLOR;
                }

            }
        }

    }
};


