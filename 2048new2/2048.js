$(function () {
    game_2048 = new Game_2048(".fw");
    game_2048.start_game();
});


function Game_2048(parameter) {
    this.container = $(parameter);
    //获取加分位置
    this.goal = this.container.find(".fw_score")
    //获取li
    this.$ctrl_li = this.container.find("li");
    //获取重新开始
    this.again = this.container.find("input")
    this.aspect = "";

    //生成随机数的函数
    this.getRandom = function (n) {
        return Math.floor(Math.random() * n + 1)
    }
    //生产数字2或者4
    this.random_number_2and4 = function () {
        var number = this.getRandom(2);
        var random_number;   //生成的数字2或者4
        if (number == 1) {
            random_number = 2;
        } else {
            random_number = 4;
        }
        ;
        return random_number;
    }
    //空白处生成1个随机数
    this.render_a_block_in_empty = function () {
        var seat_arr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],      //一个固定数组
            seat = parseInt(seat_arr.length*Math.random());     //随机生成一个数组下标
        var i = true
        while(i == true && seat_arr.length != 0){
            var $ctrl_li = $(this.$ctrl_li[seat_arr[seat]]),        //获取随机数字的位置也就是随机获取li
                $ctrl_li_text = $ctrl_li.text();                    //获取li的值
            seat_arr.splice(seat,1);
            if($ctrl_li_text == ""){
                $ctrl_li.text(this.random_number_2and4());   //赋值操作
                i = false;
            }else if($ctrl_li_text != ""){                      //随机数字位置不等于空我就要再次生成一个随机数的位置
                seat = parseInt(seat_arr.length*Math.random());
            }
        };
        if(seat_arr.length == 0 && this.equal() == true){
            alert("游戏结束");
        }

    };
    //空白处随机生成2个随机数
    this.default_number = function(){
        this.render_a_block_in_empty();    //空白处生成1个随机数
        this.render_a_block_in_empty();
    }


    //更改颜色
    this.change_color = function () {
        var text= ["",2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768]
        var text1;
        var color= ["#CCC1B3","#EDE5D9","#EDE57E","#F4B075","#F4B099","#F97C5A","#F75C31",
            "#EECD6B","#E7CC63","#E7CA4C","#E9C437","#E6C224","#AF83A6","#AD71A7","#995795","#995795"]
        for (var i = 0; i < this.$ctrl_li.length; i++) {
            var li_text = $(this.$ctrl_li[i]).text();
            if(li_text != ""){
                for(var y = 0; y < text.length; y++){
                    text1 = text[y];
                    color_y = y;
                    if(li_text == text1){
                        $(this.$ctrl_li[i]).css({
                            background : color[color_y]
                        })
                    }
                }

            }else{
                $(this.$ctrl_li[i]).css({
                    background : color[0]
                })
            }
        }
    }
    this.mergeNumber = function(){

        for(var i = 0; i < 4; i++){
            var row = [];
            var $li_4 = 0,
                $li_3 = 0,
                $li_2 = 0,
                $li_1 = 0;
            //判断方向
            if(this.aspect == "left"){
                row = [this.$ctrl_li[i * 4], this.$ctrl_li[i * 4 + 1],
                    this.$ctrl_li[i * 4 + 2], this.$ctrl_li[i * 4 + 3]];
                $li_4 = $(row[3]);
                $li_3 = $(row[2]);
                $li_2 = $(row[1]);
                $li_1 = $(row[0]);
            }else if(this.aspect == "top"){
                row = [this.$ctrl_li[i], this.$ctrl_li[i+4],
                    this.$ctrl_li[i+8], this.$ctrl_li[i+12]];
                $li_4 = $(row[3]);
                $li_3 = $(row[2]);
                $li_2 = $(row[1]);
                $li_1 = $(row[0]);
            }else if(this.aspect == "right"){
                row = [this.$ctrl_li[i * 4], this.$ctrl_li[i * 4 + 1],
                    this.$ctrl_li[i * 4 + 2], this.$ctrl_li[i * 4 + 3]];
                $li_1 = $(row[3]);
                $li_2 = $(row[2]);
                $li_3 = $(row[1]);
                $li_4 = $(row[0]);
            }else if(this.aspect == "bottom"){
                row = [this.$ctrl_li[i], this.$ctrl_li[i+4],
                    this.$ctrl_li[i+8], this.$ctrl_li[i+12]];
                $li_1 = $(row[3]);
                $li_2 = $(row[2]);
                $li_3 = $(row[1]);
                $li_4 = $(row[0]);
            }

            var $text_1 = Number($li_1.text()),
                $text_2 = Number($li_2.text()),
                $text_3 = Number($li_3.text()),
                $text_4 = Number($li_4.text());
            //当前分数
            var current = Number(this.goal.text());
            //数字相加判断
            if($text_1 == $text_2 && $text_1 != ""){
                $li_1.text($text_1 + $text_2);
                $li_2.text("");
                this.goal.text(current + $text_2);
                if($text_3 == $text_4 && $text_3 != ""){
                    $li_3.text($text_3 + $text_4);
                    $li_4.text("");
                    this.goal.text(current + $text_2);
                }
            }else if($text_2 == $text_3 && $text_2 != ""){
                $li_2.text($text_2 + $text_3);
                $li_3.text("");
                this.goal.text(current + $text_2);
            }else if($text_3 == $text_4 && $text_3 != ""){
                $li_3.text($text_4 + $text_3);
                $li_4.text("");
                this.goal.text(current + $text_2);
            }
        }


    }
    this.moveTo = function(){
        for(var i = 0; i < 4; i++){
            var row = [];
            var $li_4 = 0,
                $li_3 = 0,
                $li_2 = 0,
                $li_1 = 0;
            //判断方向
            if(this.aspect == "left"){
                row = [this.$ctrl_li[i * 4], this.$ctrl_li[i * 4 + 1],
                    this.$ctrl_li[i * 4 + 2], this.$ctrl_li[i * 4 + 3]];
                $li_4 = $(row[3]);
                $li_3 = $(row[2]);
                $li_2 = $(row[1]);
                $li_1 = $(row[0]);
            }else if(this.aspect == "top"){
                row = [this.$ctrl_li[i], this.$ctrl_li[i+4],
                    this.$ctrl_li[i+8], this.$ctrl_li[i+12]];
                $li_4 = $(row[3]);
                $li_3 = $(row[2]);
                $li_2 = $(row[1]);
                $li_1 = $(row[0]);
            }else if(this.aspect == "right"){
                row = [this.$ctrl_li[i * 4], this.$ctrl_li[i * 4 + 1],
                    this.$ctrl_li[i * 4 + 2], this.$ctrl_li[i * 4 + 3]];
                $li_1 = $(row[3]);
                $li_2 = $(row[2]);
                $li_3 = $(row[1]);
                $li_4 = $(row[0]);
            }else if(this.aspect == "bottom"){
                row = [this.$ctrl_li[i], this.$ctrl_li[i+4],
                    this.$ctrl_li[i+8], this.$ctrl_li[i+12]];
                $li_1 = $(row[3]);
                $li_2 = $(row[2]);
                $li_3 = $(row[1]);
                $li_4 = $(row[0]);
            }

            var $text_1 = Number($li_1.text()),
                $text_2 = Number($li_2.text()),
                $text_3 = Number($li_3.text()),
                $text_4 = Number($li_4.text());

            if($text_1 == 0){      //移动判断功能
                if($text_2 == 0){
                    if($text_3 == 0){
                        if($text_4 != 0){
                            $li_1.text($text_4);
                            $li_4.text("");
                        }
                    }else{
                        $li_1.text($text_3);
                        $li_3.text("");
                        if($text_4 != 0){
                            $li_2.text($text_4);
                            $li_4.text("");
                        }
                    }
                }else{
                    $li_1.text($text_2);
                    $li_2.text("");
                    if($text_3 == 0){
                        if($text_4 != 0){
                            $li_2.text($text_4);
                            $li_4.text("");
                        }
                    }else{
                        $li_2.text($text_3);
                        $li_3.text("");
                        if($text_4 != 0){
                            $li_3.text($text_4);
                            $li_4.text("");
                        }
                    }

                }
            }else{
                if($text_2 == 0){
                    if($text_3 == 0){
                        if($text_4 != 0){
                            $li_2.text($text_4);
                            $li_4.text("");
                        }
                    }else{
                        $li_2.text($text_3);
                        $li_3.text("");
                        if($text_4 != 0){
                            $li_3.text($text_4);
                            $li_4.text("");
                        }
                    }
                }else{
                    if($text_3 == 0){
                        if($text_4 != 0){
                            $li_3.text($text_4);
                            $li_4.text("");
                        }
                    }
                }
            }

        }

    }
    //向左
    this.leftHandler = function(){
        this.moveTo();
        this.mergeNumber();
        this.moveTo();
        this.render_a_block_in_empty();
        this.equal();
    }
    //判断相邻数字是否相等
    this.equal = function(){
        var f = false
        F = false;
        var w = new Array();
        //纵向判断
        for (var j = 0; j < 4; j++) {
            for (var i = 0; i < 4; i++) {
                w[i] = this.$ctrl_li[4 * j + i];
            }
            f = ($(w[0]).text() != $(w[1]).text() && $(w[1]).text() !=
                $(w[2]).text() && $(w[2]).text() != $(w[3]).text());// 如果为真,表示相邻的两个数不相等
            if (!f) {
                break;
            }
        }
        //横向判断
        for (var j = 0; j < 4; j++) {
            for (var i = 0; i < 4; i++) {
                w[i] = this.$ctrl_li[4 * i + j];
            }
            F = ($(w[0]).text() != $(w[1]).text() && $(w[1]).text() !=
                $(w[2]).text() && $(w[2]).text() != $(w[3]).text());// 如果为真,表示相邻的两个数不相等
            if (!F) {
                break;
            }
        }
        if(f == true && F == true){
            return f = true;
        }else{
            return f = false
        }
    }
    //方向键事件监听
    this.bindEvents = function(){
        var self = this;
        $(document).keydown(function(event){
            var e = event || window.event;
            var k = e.keyCode || e.which;
            switch(k) {
                case 37:
                    self.aspect = "left";
                    self.leftHandler();

                    break;
                case 38:
                    self.aspect = "top";
                    self.leftHandler();
                    break;
                case 39:
                    self.aspect = "right";
                    self.leftHandler();
                    break;
                case 40:
                    self.aspect = "bottom";
                    self.leftHandler();
                    break;
            }
            self.change_color();
            return false;
        });
    }
    //开始游戏
    this.start_game = function(){
        this.default_number();
        this.bindEvents();
        this.change_color();
        this.again.click(function () {
            $(window)[0].history.go(0);
        })
    }





};


//生成默认两个数字








